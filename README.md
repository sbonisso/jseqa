jSeqA
===============

![build-status](https://img.shields.io/bitbucket/pipelines/sbonisso/jseqa.svg?style=flat)
[![Coverage Status](https://coveralls.io/repos/bitbucket/sbonisso/jseqa/badge.svg?branch=master)](https://coveralls.io/bitbucket/sbonisso/jseqa?branch=master)

Java sequence analysis library (jSeqA). Provides functionality required to perform basic sequence analysis tasks, including:

1. Pairwise sequence alignment
2. Alignment free pairwise comparisons
3. FASTA/FASTQ processing
4. Sequence (nucleotide/amino acid) manipulation and property calculations
5. Basic multiple sequence alignment
