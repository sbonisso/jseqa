/**
 * Elements of sequences, both nucleotide and amino acid, are represented.
 */
package jseqa.sequence;