/**
 * The alignmentfree package allows comparisons of sequences without using pairwise alignments. Alignment-free
 * comparisons are mainly performed using {@link jseqa.alignmentfree.FeatureFrequencyProfile}s (FFP).
 * <br>
 * There are also basic similarity and distance methods in:
 * <ul>
 *     <li>Distances: distance metrics of various types in {@link jseqa.alignmentfree.DistanceMetrics}.</li>
 *     <li>Statistics: statistics from a population of numbers, e.g., mean, standard deviation,
 *     etc., in {@link jseqa.alignmentfree.DistributionStatistics}.</li>
 *     <li>Similarity: similarity measures in {@link jseqa.alignmentfree.SimilarityMeasures}.</li>
 * </ul>
 *
 */
package jseqa.alignmentfree;