package jseqa.alignment;

import jseqa.alignment.substitution.SubstitutionMatrix;

/**
 * Abstract base class for the basic pairwise alignment algorithms.
 *
 */
public abstract class AbstractBasicPairwiseAlignment extends AbstractPairwiseAlignment {

    /** 
     * An NxM matrix of scores. Where:
     * <ul>
     * <li>N=length(seqA) + 1</li>
     * <li>M=length(seqB) + 1</li>
     * </ul>
     */
    protected AlignmentMatrixScore alignScoreMatrix;

    /**
     * An NxM matrix of backtracking pointers. Where:
     * <ul>
     * <li>N=length(seqA) + 1</li>
     * <li>M=length(seqB) + 1</li>
     * </ul>
     * 
     */
    protected AlignmentMatrixBacktrack alignPtrMatrix;

    public AbstractBasicPairwiseAlignment(String inSeqA, 
                                          String inSeqB, 
                                          SubstitutionMatrix scoremat) {
        super(inSeqA, inSeqB, scoremat);
    }

    /**
     * Set the alignment matrices.
     * @param alignScoreMat {@code AlignmentMatrixScore} of scores.
     * @param alignPtrMat {@code AlignmentMatrixBacktrack} of backtracking pointers.
     */
    public void setAlignmentMatrix(AlignmentMatrixScore alignScoreMat, 
                                   AlignmentMatrixBacktrack alignPtrMat) {
        if (alignScoreMat.nrows() < this.lenSeqA) {
            throw new IllegalArgumentException("Score matrix has too few rows");
        }
        if (alignScoreMat.ncols() < this.lenSeqB) {
            throw new IllegalArgumentException("Score matrix has too few cols");
        }
        this.alignScoreMatrix = alignScoreMat;
        this.alignPtrMatrix = alignPtrMat;
    }

    @Override
    public void initMatrix() {
        this.alignScoreMatrix.clearSubMatrix(this.lenSeqA, this.lenSeqB);
        this.alignPtrMatrix.clearSubMatrix(this.lenSeqA, this.lenSeqB);
    }

    /**
     * {@inheritDoc}
     * <br>
     * Fill in the each cell using the global alignment (Needleman-Wunch) recurrence relation, found
     * at {@link GlobalAlignment}.
     */
    @Override
    public void fillCell(int i, int j) {
        char aCh = this.inputSeqA.charAt(i - 1);
        char bCh = this.inputSeqB.charAt(j - 1);

        int insScore = this.alignScoreMatrix.get(i - 1, j) + this.scoreMatrix.gapPenalty();
        int delScore = this.alignScoreMatrix.get(i, j - 1) + this.scoreMatrix.gapPenalty();
        int diagScore = this.alignScoreMatrix.get(i - 1, j - 1) + this.scoreMatrix.score(aCh, bCh);

        int retval = this.argmax3(diagScore, insScore, delScore);
        BacktrackPtr ptr = null;
        int score = 0;
        if (retval == 0) {
            score = diagScore;
            ptr = BacktrackPtr.DIAG;
        } else if (retval == 1) {
            score = insScore;
            ptr = BacktrackPtr.UP;
        } else {
            score = delScore;
            ptr = BacktrackPtr.LEFT;
        }
        this.alignScoreMatrix.set(i, j, score);
        this.alignPtrMatrix.set(i, j, ptr);
    }

    @Override
    protected abstract void setBacktrackCell();

    /**
     * Find the cell that would be the start of backtracking, and report the score.
     * @return int score of alignment.
     */
    public int getScore() {
        this.setBacktrackCell();
        return this.alignScoreMatrix.get(this.endCell.first(), this.endCell.second());
    }

    /**
     * Backtracks from the end cell of the extending class.
     */
    @Override
    public Alignment backtrack() {
        this.setBacktrackCell();
        int score = this.alignScoreMatrix.get(this.endCell.first(), this.endCell.second());
        this.currAlign = this.recursiveBacktrack(this.endCell.first(), 
                                                 this.endCell.second(), 
                                                 new AlignmentBuilder(score));
        return this.currAlign;
    }

    @Override
    protected Alignment recursiveBacktrack(int i, int j, AlignmentBuilder aln) {
        if (i <= 0 && j <= 0) {
            return aln.toAlignment();
        }
        BacktrackPtr ptr = this.alignPtrMatrix.get(i, j);
        if (ptr == BacktrackPtr.DIAG) {
            char aCh = this.inputSeqA.charAt(i - 1);
            char bCh = this.inputSeqB.charAt(j - 1);
            aln.addColumn(aCh, bCh);
            return this.recursiveBacktrack(i - 1, j - 1, aln);
        } else if (ptr == BacktrackPtr.UP) {
            char aCh = this.inputSeqA.charAt(i - 1);
            aln.addColumn(aCh, '-');
            return this.recursiveBacktrack(i - 1, j, aln);
        } else if (ptr == BacktrackPtr.LEFT) {
            char bCh = this.inputSeqB.charAt(j - 1);
            aln.addColumn('-', bCh);
            return this.recursiveBacktrack(i, j - 1, aln);
        } else {
            return aln.toAlignment();
        }
    }

}
