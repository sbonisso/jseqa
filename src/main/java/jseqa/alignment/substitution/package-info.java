/**
 * Substitution matrices for pairwise alignments. At the minimum these include mismatch penalties and indel penalties.
 * Addtitionally, a gap extension penalty is provided for affine alignment types.
 *
 */
package jseqa.alignment.substitution;